
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "dbl_api_ruby/version"

Gem::Specification.new do |spec|
  spec.name                  = "dbl_api"
  spec.version               = DBL::VERSION
  spec.authors               = ["Feña Agar"]
  spec.email                 = ["fernando.agar@gmail.com"]

  spec.summary               = %q{Discord Bot List API}
  spec.description           = %q{Discord Bot List API}
  spec.homepage              = "https://github.com/elfenars/dbl_api_ruby"
  spec.license               = "MIT"
  spec.required_ruby_version = '~> 2.0'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "pry-byebug"
end
